console.log("Hello World!");


let firstNum = 2;

let getCube = firstNum ** 3;

console.log(`The cube of ${firstNum} is ${getCube}`);


let address = ["258 Washington Ave NW", "258 Washington Ave NW", 90011];

let [streetName, stateName, postalcode] = address;
console.log(`I live at ${streetName}, ${stateName} ${postalcode}`);


let animal = {
	name: "Lolong",
	type: "crocodile",
	weight: 1075,
	measurement: "20 ft 3 in"
}

let {name, type, weight, measurement} = animal;
console.log(`${name} was a salt water ${type}. He weighed at ${weight} kgs with a measurement of ${measurement}.`);


let numbers = [1, 2, 3, 4, 5];

numbers.forEach((list) => {
	console.log(`${list}`);
})

let reducedArray = numbers.reduce((acc, curr) => {
	return acc + curr;

})
console.log(reducedArray);



class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

let myDog = new Dog("Frankie", 5, "Miniature Dachshund");
console.log(myDog);
